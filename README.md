# OpenML dataset: Concrete_Data

https://www.openml.org/d/4353

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Concrete is the most important material in civil engineering. The concrete compressive strength is a highly nonlinear function of age and ingredients. These ingredients include cement, blast furnace slag, fly ash, water, superplasticizer, coarse aggregate, and fine aggregate.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/4353) of an [OpenML dataset](https://www.openml.org/d/4353). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/4353/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/4353/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/4353/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

